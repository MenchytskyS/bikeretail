package com.epam.bikeRetail.tag;

import com.epam.bikeRetail.entity.Bike;
import com.epam.bikeRetail.entity.RentBike;
import com.epam.bikeRetail.entity.User;
import com.epam.bikeRetail.exception.ServiceException;
import com.epam.bikeRetail.service.BikeService;
import com.epam.bikeRetail.service.UserService;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class CustomTag extends TagSupport {

    private String id;

    /**
     * Sets id.
     *
     * @param id the id.
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int doStartTag() throws JspException {
        RentBike rentBike;
        Bike bike;
        try{
            BikeService bikeService = new BikeService();
            int userId = Integer.parseInt(id);
            rentBike = bikeService.findRentBike(userId);

            bike = bikeService.findBikeById(rentBike.getBikeId());

            JspWriter writer = pageContext.getOut();
            writer.write(String.format("%s %s", bike.getBrand(), bike.getModel()  ));
        } catch (IOException | ServiceException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

}

