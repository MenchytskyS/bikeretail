package com.epam.bikeRetail.service;

import com.epam.bikeRetail.dao.BikeDAO;
import com.epam.bikeRetail.dao.BikeStationDAO;
import com.epam.bikeRetail.dao.DAOCreator;
import com.epam.bikeRetail.dao.RentBikeDAO;
import com.epam.bikeRetail.entity.BikeStation;
import com.epam.bikeRetail.entity.Bike;
import com.epam.bikeRetail.entity.RentBike;
import com.epam.bikeRetail.exception.ConnectionException;
import com.epam.bikeRetail.exception.DAOException;
import com.epam.bikeRetail.exception.ServiceException;

import java.util.List;

/**
 * Service class for Bike entity.
 *
 * @author Stepan Menchytcky
 * @see Bike
 */
public class BikeService {
    /**
     * Method find bikes by station id in dataBase.
     *
     * @param stationId Station id.
     * @return List of bikes on station.
     * @throws ServiceException when SQLException and DAOException detected.
     */
    public List<Bike> showBikesOnStation(int stationId) throws ServiceException {
        List<Bike> bikes;

        try (DAOCreator daoCreator = new DAOCreator()) {

            BikeDAO bikeDAO = daoCreator.getBikeDAO();

            bikes = bikeDAO.getAllById(stationId);
        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }

        return bikes;
    }

    /**
     * Method find all bikes in dataBase.
     *
     * @return List of bikes on station.
     * @throws ServiceException when SQLException and DAOException detected.
     */
    public List<Bike> showAllBikes() throws ServiceException {
        List<Bike> bikes;

        try (DAOCreator daoCreator = new DAOCreator()) {

            BikeDAO bikeDAO = daoCreator.getBikeDAO();

            bikes = bikeDAO.getAll();
        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }

        return bikes;
    }

    public List<Bike> showAllBikesByPages(int offSet, int numberOfRecords) throws ServiceException {
        List<Bike> bikes;
        try (DAOCreator daoCreator = new DAOCreator()) {

            BikeDAO bikeDAO = daoCreator.getBikeDAO();

            bikes = bikeDAO.selectAllBikesByFoundRows(offSet, numberOfRecords);
        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }

        return bikes;
    }
    /**
     * Method to add bike to the station and dataBase.
     *
     * @param bike Entity.
     * @param stationId Id of the station where need to add bike.
     * @throws ServiceException when SQLException and DAOException detected.
     */
    public void addBike(Bike bike, int stationId) throws ServiceException {
        try (DAOCreator daoCreator = new DAOCreator()) {
            daoCreator.startTransaction();

            BikeDAO bikeDAO = daoCreator.getBikeDAO();
            BikeStationDAO bikeStationDAO = daoCreator.getBikeStationDAO();

            bikeDAO.insert(bike);

            BikeStation bikeStation = new BikeStation();
            bikeStation.setStationId(stationId);
            bikeStation.setBikeId(bike.getId());

            bikeStationDAO.insert(bikeStation);

            daoCreator.commitTransaction();

        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }
    }

    /**
     * Method to delete bike from dataBase.
     *
     * @param bikeId Id of deleted bike.
     * @param bikeStation Data about what bike on which station.
     * @throws ServiceException When SQLException and DAOException detected.
     */
    public void deleteBike(int bikeId, BikeStation bikeStation) throws ServiceException {

        try (DAOCreator daoCreator = new DAOCreator()) {
            daoCreator.startTransaction();

            BikeDAO bikeDAO = daoCreator.getBikeDAO();
            BikeStationDAO bikeStationDAO = daoCreator.getBikeStationDAO();

            bikeStationDAO.delete(bikeStation);

            Bike bike = bikeDAO.getById(bikeId);
            bikeDAO.delete(bike);

            daoCreator.commitTransaction();
        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }
    }

    /**
     * Method to move bike to another station.
     *
     * @param bikeStation Data about what bike on which station.
     * @throws ServiceException When SQLException and DAOException detected.
     */
    public void moveBike(BikeStation bikeStation) throws ServiceException {
        try (DAOCreator daoCreator = new DAOCreator()) {

            BikeStationDAO bikeStationDAO = daoCreator.getBikeStationDAO();

            bikeStationDAO.update(bikeStation);
        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }
    }

    /**
     * Method to find bike by id.
     *
     * @param bikeId bike Id.
     * @throws ServiceException When SQLException and DAOException detected.
     */
    public Bike findBikeById(int bikeId) throws ServiceException {
        Bike bike;

        try (DAOCreator daoCreator = new DAOCreator()) {
            BikeDAO bikeDAO = daoCreator.getBikeDAO();

            bike = bikeDAO.getById(bikeId);
        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }

        return bike;
    }

    public void editBike(Bike bike) throws ServiceException {
        try (DAOCreator daoCreator = new DAOCreator()) {

            BikeDAO bikeDAO = daoCreator.getBikeDAO();

            bikeDAO.update(bike);

        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }
    }


    public RentBike findRentBike(int userId) throws ServiceException {
        RentBike rentBike;

        try (DAOCreator daoCreator = new DAOCreator()) {

            RentBikeDAO rentBikeDAO = daoCreator.getRentBikeDAO();

            rentBike = rentBikeDAO.getById(userId);
        } catch (DAOException | ConnectionException e) {
            throw new ServiceException("SQLException and DAOException detected", e);
        }

        return rentBike;
    }
}
