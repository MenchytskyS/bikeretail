package com.epam.bikeRetail.command.admin;

import com.epam.bikeRetail.command.ActionCommand;
import com.epam.bikeRetail.entity.Bike;
import com.epam.bikeRetail.exception.ServiceException;
import com.epam.bikeRetail.resource.ConfigurationManager;
import com.epam.bikeRetail.resource.MessageManager;
import com.epam.bikeRetail.service.BikeService;
import com.epam.bikeRetail.utils.BikeDataValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

/**
 * Command to edit bike.
 *
 * @author Stepan Menchytsky
 * @see ActionCommand
 * @see HttpServletRequest
 */
public class EditBikeCommand implements ActionCommand {
    private static final Logger LOGGER = LogManager.getLogger(EditBikeCommand.
            class.getName());
    private static final String PATH_PAGE_OPERATION_DONE = "path.page.operationDone";
    private static final String ERROR_PAGE = "path.page.error";
    private static final String INVALID_DATA_MESSAGE_PATH = "message.notValid";
    private static final String BRAND = "brand";
    private static final String MODEL = "model";
    private static final String BIKE_ID = "bikeId";
    private static final String PRICE_ON_HOUR = "priceOnHour";
    private static final String RESULT_ATTRIBUTE = "result";

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;

        try {
            String brand = request.getParameter(BRAND);
            String model = request.getParameter(MODEL);
            String bikeIdStr = request.getParameter(BIKE_ID);
            int bikeId = Integer.parseInt(bikeIdStr);
            String bikePriceOnHour = request.getParameter(PRICE_ON_HOUR);

            BikeDataValidator bikeDataValidator = new BikeDataValidator();
            boolean isBikeDataValid = bikeDataValidator.
                    checkData(brand, model, bikePriceOnHour);

            if (isBikeDataValid) {
                BikeService bikeService = new BikeService();

                Bike bike = bikeService.findBikeById(bikeId);

                BigDecimal priceOnHour = new BigDecimal(bikePriceOnHour);
                bike.setBrand(brand);
                bike.setModel(model);
                bike.setPriceOnHour(priceOnHour);

                bikeService.editBike(bike);
                page = ConfigurationManager.getProperty(PATH_PAGE_OPERATION_DONE);
            } else {
                String property = MessageManager.
                        getProperty(INVALID_DATA_MESSAGE_PATH);
                request.setAttribute(RESULT_ATTRIBUTE, property);
            }
        } catch (ServiceException e) {
            LOGGER.error("Service exception detected.", e);
            return ConfigurationManager.getProperty(ERROR_PAGE);
        }

        return page;
    }
}
