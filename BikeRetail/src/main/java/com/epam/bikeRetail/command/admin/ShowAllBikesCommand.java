package com.epam.bikeRetail.command.admin;

import com.epam.bikeRetail.command.ActionCommand;
import com.epam.bikeRetail.entity.Bike;
import com.epam.bikeRetail.exception.ServiceException;
import com.epam.bikeRetail.resource.ConfigurationManager;
import com.epam.bikeRetail.service.BikeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Command to show all bikes.
 *
 * @author Stepan Menchytsky
 * @see ActionCommand
 * @see HttpServletRequest
 */
public class ShowAllBikesCommand implements ActionCommand {
    private static final Logger LOGGER = LogManager.getLogger(ShowAllBikesCommand.
            class.getName());
    private final static String BIKES_PAGE = "path.page.bikes";
    private final static String LIST_ATTRIBUTE = "bikes";
    private final static String ERROR_PAGE = "path.page.error";
    private final static int MAX_RECORDS_PER_PAGE_COUNT = 5;
    private final static int FIRST_PAGE_INDEX = 1;
    private final static String PAGE_PARAMETER = "page";
    private final static String NUMBER_OF_PAGE_ATTRIBUTE = "numberOfRecords";
    private final static String CURRENT_PAGE_INDEX_ATTRIBUTE = "pageIndex";

    /**
     * Implementation of command to show all bikes.
     *
     * @param request HttpServletRequest object.
     * @return redirect page.
     */
    @Override
    public String execute(HttpServletRequest request) {

        int pageIndex = FIRST_PAGE_INDEX;
        String page;
        List<Bike> bikes = null;

        BikeService bikeService = new BikeService();

        try {
            String pageParameterValue = request.getParameter(PAGE_PARAMETER);

            if (pageParameterValue != null) {
                pageIndex = Integer.parseInt(pageParameterValue);
            }

            int currentOffSet = (pageIndex - 1) * MAX_RECORDS_PER_PAGE_COUNT;

//            bikes = bikeService.showAllBikes();
            bikes = bikeService.showAllBikesByPages(currentOffSet, MAX_RECORDS_PER_PAGE_COUNT );

            int numberOfRecords = bikes.size();

            request.setAttribute(NUMBER_OF_PAGE_ATTRIBUTE, numberOfRecords);
            request.setAttribute(CURRENT_PAGE_INDEX_ATTRIBUTE, pageIndex);
            request.setAttribute(LIST_ATTRIBUTE, bikes);

            page = ConfigurationManager.getProperty(BIKES_PAGE);

            return page;
        } catch (ServiceException e) {
            LOGGER.error("Service exception detected.", e);
            return ConfigurationManager.getProperty(ERROR_PAGE);
        }
    }
}
