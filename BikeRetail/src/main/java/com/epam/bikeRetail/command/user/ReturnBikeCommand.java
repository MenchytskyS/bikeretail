package com.epam.bikeRetail.command.user;

import com.epam.bikeRetail.command.ActionCommand;
import com.epam.bikeRetail.entity.User;
import com.epam.bikeRetail.exception.ServiceException;
import com.epam.bikeRetail.resource.ConfigurationManager;
import com.epam.bikeRetail.resource.MessageManager;
import com.epam.bikeRetail.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ReturnBikeCommand implements ActionCommand{
    private static final int RENT_STATUS_OFF = 0;
    private final Logger LOGGER = LogManager.getLogger(ReturnBikeCommand.class.getName());
    private final static String USER_PAGE = "path.page.user";
    private final static String ERROR_PAGE = "path.page.error";
    private final static String USER_STATION_PAGE = "path.page.userStation";
    private final static String USER_ATTRIBUTE = "user";
    private static final String ERROR_MESSAGE_PATH = "message.returnBikeError";
    private static final String ERROR_ATTRIBUTE = "returnBikeError";

    @Override
    public String execute(HttpServletRequest request) {
        String page;

        String stationIdStr = request.getParameter("stationId");
        int stationId = Integer.parseInt(stationIdStr);
        UserService userService = new UserService();

        try {
            HttpSession currentSession = request.getSession();
            User user = (User) currentSession.getAttribute("user");
            user.setRentStatus(RENT_STATUS_OFF);

            boolean isOperationSuccessful = userService.returnBike(stationId, user);
            if (!isOperationSuccessful){
                String errorMessage = MessageManager.
                        getProperty(ERROR_MESSAGE_PATH);
                request.setAttribute(ERROR_ATTRIBUTE, errorMessage);

                page = ConfigurationManager.getProperty(USER_STATION_PAGE);
            }
            currentSession.setAttribute(USER_ATTRIBUTE, user);

            page = ConfigurationManager.getProperty(USER_PAGE);
        } catch (ServiceException e){
            LOGGER.error("Service exception detected.", e);
            return ConfigurationManager.getProperty(ERROR_PAGE);
        }

        return page;
    }
}
