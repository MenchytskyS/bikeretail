<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="page_content">
    <fmt:message key="menu.bike_brand" var="brand"/>
    <fmt:message key="menu.bike_model" var="model"/>
    <fmt:message key="menu.price_on_hour" var="price"/>
    <fmt:message key="menu.add_bike" var="add"/>
    <fmt:message key="menu.station_Red" var="red"/>
    <fmt:message key="menu.station_Green" var="green"/>
    <fmt:message key="menu.station_Blue" var="blue"/>
    <fmt:message key="menu.station_Black" var="black"/>
    <fmt:message key="menu.station" var="Station"/>
    <fmt:message key="menu.delete_bike" var="delete"/>
    <fmt:message key="admin.operation" var="operation"/>
</fmt:bundle>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great|Karma|Pompiere|Rokkitt" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin-style.css">
    <title>BIKETRO Admin Page</title>
</head>

<body>
<tag:adminMenu/>
<p>${operation}</p>
</body>
</html>
