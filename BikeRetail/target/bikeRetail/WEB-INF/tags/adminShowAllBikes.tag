<%@tag pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@attribute name="bikes" required="true" type="java.util.List" %>

<fmt:bundle basename="page_content">
    <fmt:message key="menu.bike_brand" var="brand"/>
    <fmt:message key="menu.bike_model" var="model"/>
    <fmt:message key="menu.price_on_hour" var="price"/>
    <fmt:message key="menu.add_bike" var="add"/>
    <fmt:message key="menu.station_Red" var="red"/>
    <fmt:message key="menu.station_Green" var="green"/>
    <fmt:message key="menu.station_Blue" var="blue"/>
    <fmt:message key="menu.station_Black" var="black"/>
    <fmt:message key="menu.station" var="Station"/>
    <fmt:message key="menu.delete_bike" var="delete"/>
    <fmt:message key="admin.edit_bike" var="editeB"/>
    <fmt:message key="admin.ok" var="ok"/>
</fmt:bundle>

<table class='table table-hover'>
    <thead class='thead-dark'>
    <tr>
        <th>${brand}</th>
        <th>${model}</th>
        <th>${price}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${bikes}" var="bike">
        <tr>
            <td>${bike.brand}</td>
            <td>${bike.model}</td>
            <td>${bike.priceOnHour}</td>
            <td>
                <a href="#${bike.id}" class="table-button" data-toggle="collapse">${editeB}</a>
                <form id="${bike.id}" class="collapse" name="editBikeForm" method="POST" action="${pageContext.request.contextPath}/Controller">
                        <input type="hidden" name="command" value="admin_edit_bike"/>
                        <p>${brand} ${bike.brand}</p>
                        <input type="text" name="brand" value="" required pattern="#[a-zA-Z0-9_]+{6,}" />
                        <p> ${model} ${bike.model}</p>
                        <input type="text" name="model" value="" required pattern="#[a-zA-Z0-9_]+{6,}" />
                        <p> ${price} ${bike.priceOnHour} </p>
                        <input type="text" name="priceOnHour" value="" required pattern="#\d+\.\d+" />
                        <input type="hidden" name="bikeId" value="${bike.id}"/>
                        <input type="submit" value=${ok}>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
