<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="tag" tagdir="/WEB-INF/tags" %>

<fmt:bundle basename="page_content">
    <fmt:message key="menu.bike_brand" var="brand"/>
    <fmt:message key="menu.bike_model" var="model"/>
    <fmt:message key="menu.price_on_hour" var="price"/>
    <fmt:message key="menu.add_bike" var="add"/>
    <fmt:message key="menu.station_Red" var="red"/>
    <fmt:message key="menu.station_Green" var="green"/>
    <fmt:message key="menu.station_Blue" var="blue"/>
    <fmt:message key="menu.station_Black" var="black"/>
    <fmt:message key="menu.station" var="Station"/>
    <fmt:message key="menu.delete_bike" var="delete"/>
    <fmt:message key="admin.previous" var="previous"/>
    <fmt:message key="admin.next" var="next"/>
</fmt:bundle>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great|Karma|Pompiere|Rokkitt" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/normalize.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/admin-style.css">
    <title>BIKETRO Admin Page</title>
</head>

<body>
<tag:adminMenu/>
<div class="container-center">
    <div>
    <a href='#' class="button" data-toggle="modal" data-target="#myModal">${add}</a>
    </div>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">${add}</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form name="addBikeForm" method="POST" action="${pageContext.request.contextPath}/Controller">
                        <input type="hidden" name="command" value="admin_add_bike" />
                        <p>${brand}</p>
                        <input type="text" name="brand" value="" required pattern="#[a-zA-Z0-9_]+{6,}" />
                        <p> ${model}</p>
                        <input type="text" name="model" value=""  required pattern="#[a-zA-Z0-9_]+{6,}" />
                        <p> ${price} </p>
                        <input type="text" name="priceOnHour" value="" required pattern="#\d+\.\d+"/>
                        <br/>
                        <select name="stationId">
                            <option value="1">${red}</option>
                            <option value="2">${green}</option>
                            <option value="3">${blue}</option>
                            <option value="4">${black}</option>
                        </select>
                        <input type="submit" value=${add} />
                    </form>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="button" data-dismiss="modal">${close}</button>
                </div>

            </div>
        </div>
    </div>
<tag:adminShowAllBikes bikes="${bikes}"/>
    <div class="pagination-button">
        <ul>
            <c:if test="${pageIndex != 1}">
                <li>
                    <a href="Controller?command=admin_show_all_bikes&page=${pageIndex - 1}">${previous}</a>
                </li>
            </c:if>
            <c:if test="${ numberOfRecords == 5}">
                <li>
                    <a href="Controller?command=admin_show_all_bikes&page=${pageIndex + 1}">${next}</a>
                </li>
            </c:if>
        </ul>
    </div>
</div>
</body>
</html>
